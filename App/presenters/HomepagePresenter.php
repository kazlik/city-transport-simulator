<?php

namespace App\Presenters;

class HomepagePresenter extends \Nette\Application\UI\Presenter
{

    /** @var \App\Map\MapGenerator @inject */
    public $mapGenerator;

    public function renderDefault()
    {
        $this->template->map = $this->mapGenerator->generate();
    }

}
