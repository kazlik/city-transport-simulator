<?php

namespace App\Map;

/**
 * Description of MapGenerator
 *
 * @author Marek
 *
 *  16  1  64
 *    \ | /
 *  4 -	  -	8
 *    /	| \
 *  128 2  32
 */
class MapGenerator
{

    private $map = [];

    public function generate($width = 10, $height = 10)
    {
        //echo '<style>body{font-family: Monospace;}</style>';
        for ($h = 0; $h < $height; $h++)
        {
            for ($w = 0; $w < $width; $w++)
            {
                echo $this->generateMapTile($w, $h);
            }
            //echo "<br>";
        }
        return $this->map;
    }

    private function generateMapTile($x, $y)
    {
        //dump($x . ' ' . $y);
        $number = $this->generateNumber();

        if ($this->getMapTile($x, $y - 1) & 2)
        {
            $this->addBit($number, 1);
        } else
        {
            $this->removeBit($number, 1);
        }
        if ($this->getMapTile($x - 1, $y) & 8)
        {
            $this->addBit($number, 4);
        } else
        {
            $this->removeBit($number, 4);
        }
        if ($this->getMapTile($x - 1, $y - 1) & 32)
        {
            $this->addBit($number, 16);
        } else
        {
            $this->removeBit($number, 16);
        }
        if ($this->getMapTile($x + 1, $y - 1) & 128)
        {
            $this->addBit($number, 64);
        } else
        {
            $this->removeBit($number, 64);
        }


        switch ($number)
        {
            case 0:
            case 1:
            case 2:
            case 4:
            case 8:
            case 16:
            case 32:
            case 64:
            case 128:
            // Nemuzou byt cesty o 45° jen
            case 65:
            case 72:
            case 40:
            case 34:
            case 130:
            case 132:
            case 20:
            case 17:
            // Nemuzou byt cesty o 90° jen
            case 9:
            case 96:
            case 10:
            case 160:
            case 6:
            case 144:
            case 5:
            case 80:
            // Nemuzou byt vsechny 3 cesty v 45° uhlu navazujici
            case 73:
            case 104:
            case 42:
            case 162:
            case 134:
            case 148:
            case 21:
            case 81:
                return $this->generateMapTile($x, $y);
                $number = 0;
                break;
        }
        $this->map[$y][$x] = $number;
        return;
    }

    private function getMapTile($x, $y)
    {
        //dump($x . ' ' . $y . ': ' . (isset($this->map[$x][$y]) ? $this->map[$x][$y] : 0));
        return isset($this->map[$y][$x]) ? $this->map[$y][$x] : $this->generateNumber();
    }

    private function addBit(&$number, $position)
    {
        $number |= $position;
    }

    private function removeBit(&$number, $position)
    {
        if ($number & $position)
        {
            $number ^= $position;
        }
    }

    private function generateNumber()
    {
        $number  = 0;
        $active  = 0;
        $numbers = [];
        for ($i = 1; $i < 256; $i = $i * 2)
        {
            $numbers[] = $i;
        }
        shuffle($numbers);
        for ($i = 0; $i < sizeof($numbers); $i++)
        {
            if ($active >= 2)
            {
                break;
            }
            if (rand(0, 1) > 0)
            {
                $number += $numbers[$i];
                $active++;
            }
        }
        return $number;
        return rand(0, 255);
    }

}
